Bootstrap: docker
From: ubuntu:20.04

%labels
    Author IFB
    Version 5.54-87.0

%files
    /src/interproscan/5.54-87.0/smart_01_06_2016.tar.gz /opt
    /src/interproscan/5.54-87.0/signalp-4.1c.Linux.tar.gz /opt
    /src/interproscan/5.54-87.0/tmhmm-2.0c.Linux.tar.gz /opt
    /src/interproscan/5.54-87.0/phobius101_linux.tar.gz /opt

%post
    export INTERPROSCAN_VERSION=5.54-87.0

    export PATH=/opt/conda/bin:$PATH

    # Install dependencies
    ## From apt
    apt-get -qq update && apt-get -qq upgrade -y
    apt-get -qq install -y wget tar locales
    apt-get -qq clean && rm -rf /var/lib/apt/lists/*

    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

    ## From conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    rm Miniconda3-latest-Linux-x86_64.sh

    conda update -q -y conda
    conda install -q -c conda-forge -y mamba
    mamba install -q -y -c conda-forge -c bioconda -c defaults\
          conda-forge::perl \
          conda-forge::openjdk=11.0.9.1 \
          bioconda::perl-findbin \
          bioconda::perl-getopt-long \
          bioconda::perl \
          conda-forge::libgomp \
          bioconda::pftools

    conda clean -a -y -q

    # INTERPROSCAN
    cd /opt/
    wget -q ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/${INTERPROSCAN_VERSION}/interproscan-${INTERPROSCAN_VERSION}-64-bit.tar.gz
    tar -xzf interproscan-${INTERPROSCAN_VERSION}-64-bit.tar.gz
    mv interproscan-${INTERPROSCAN_VERSION} interproscan

    rm -rf interproscan-${INTERPROSCAN_VERSION}-64-bit.tar.gz

    # SMART
    cd /opt
    tar -zxf smart_01_06_2016.tar.gz
    cp smart_01_06_2016/THRESHOLDS /opt/interproscan/data/smart/

    sed -ir "s|^smart.threshold.path=.*$|smart.threshold.path=\$\{data.directory\}/smart/7.1/THRESHOLDS|" interproscan/interproscan.properties

    rm -rf /opt/smart*

    # SIGNALP
    cd /opt/
    mkdir -p /opt/interproscan/bin/signalp/4.1/
    tar -zxf signalp-4.1c.Linux.tar.gz
    mv signalp-4.1/signalp /opt/interproscan/bin/signalp/4.1/
    mv signalp-4.1/bin/ /opt/interproscan/bin/signalp/4.1/bin/
    mv signalp-4.1/lib/ /opt/interproscan/bin/signalp/4.1/lib/

    cp /opt/interproscan/bin/signalp/4.1/lib/FASTA.pm /opt/conda/lib/perl5/5.32/site_perl

    sed -i "s|/usr/cbs/bio/src/signalp-4.1|/opt/interproscan/bin/signalp/4.1|" /opt/interproscan/bin/signalp/4.1/signalp

    rm -rf /opt/signalp*

    # TMHMM
    cd /opt/
    mkdir -p /opt/interproscan/bin/tmhmm/2.0c/
    tar -zxf tmhmm-2.0c.Linux.tar.gz
    cp tmhmm-2.0c/bin/decodeanhmm.Linux_x86_64 /opt/interproscan/bin/tmhmm/2.0c/decodeanhmm
    cp tmhmm-2.0c/lib/TMHMM2.0.model /opt/interproscan/data/tmhmm/2.0c/TMHMM2.0c.model

    rm -rf /opt/tmhmm*

    # PHOBIUS
    cd /opt
    mkdir -p /opt/interproscan/bin/phobius/1.01/
    mkdir phobius
    tar -zxf phobius101_linux.tar.gz -C phobius
    find phobius -name "decodeanhmm.64bit" -exec mv {} /opt/interproscan/bin/phobius/1.01/decodeanhmm \;
    find phobius -name "phobius.model" -exec mv {} /opt/interproscan/bin/phobius/1.01/ \;
    find phobius -name "phobius.options" -exec mv {} /opt/interproscan/bin/phobius/1.01/ \;
    find phobius -name "phobius.pl" -exec mv {} /opt/interproscan/bin/phobius/1.01/ \;

    chmod +rx /opt/interproscan/bin/phobius/1.01/decodeanhmm
    chmod +rx /opt/interproscan/bin/phobius/1.01/phobius.pl
    chmod +r /opt/interproscan/bin/phobius/1.01/*

    rm -rf /opt/phobius*

    # PFTOOLS: use newer version to fix broken --affinity option
    sed -i 's|binary.prosite.psscan.pl.path=${bin.directory}/prosite|binary.prosite.psscan.pl.path=/opt/conda/bin|' /opt/interproscan/interproscan.properties
    sed -i 's|binary.prosite.pfscan.path=${bin.directory}/prosite|binary.prosite.pfscan.path=/opt/conda/bin|' /opt/interproscan/interproscan.properties
    sed -i 's|binary.prosite.pfsearch.path=${bin.directory}/prosite|binary.prosite.pfsearch.path=/opt/conda/bin|' /opt/interproscan/interproscan.properties
    sed -i 's|binary.prosite.pfscanv3.path=${bin.directory}/prosite|binary.prosite.pfscanv3.path=/opt/conda/bin|' /opt/interproscan/interproscan.properties
    sed -i 's|binary.prosite.pfsearchv3.path=${bin.directory}/prosite|binary.prosite.pfsearchv3.path=/opt/conda/bin|' /opt/interproscan/interproscan.properties
    echo "binary.pfscanv3.path=/opt/conda/bin/pfscanV3" >> /opt/interproscan/interproscan.properties
    echo "binary.pfsearchv3.path=/opt/conda/bin/pfsearchV3" >> /opt/interproscan/interproscan.properties

    # INTERPROSCAN
    ## Setting interproscan.properties
    cd /opt/interproscan/
    sed -i "s/=--cpu 1/=--cpu 4/" interproscan.properties
    sed -i "s/=-c 1/=-c 4/" interproscan.properties
    sed -i "s/=-cpu 1/=-cpu 4/" interproscan.properties
    sed -i "s/-Xmx2048m/-Xmx12288m/" interproscan.properties

    cd /opt/interproscan/bin/
    for i in $(grep -r "/usr/bin/perl" * | cut -d ":" -f 1); do sed -i "s|/usr/bin/perl|/opt/conda/bin/perl|" $i; done

    ## Installation
    cd /opt/interproscan
    df -h
    python3 initial_setup.py


%environment
    export PATH=/opt/interproscan/:/opt/conda/bin:$PATH
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/x86_64-linux-gnu/:/opt/conda/lib
    export LANG=en_US.utf8

%runscript
    echo "InterProScan depends on several software under access restriction and is only available for Academic usage"
    echo "InterProScan is setted to use 4 CPU and less 16GB of RAM. So in SLURM --cpus-per-task=4 --mem=16GB"
    exec "$@"

%test
    export PATH=/opt/interproscan/:/opt/conda/bin:$PATH
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/x86_64-linux-gnu/:/opt/conda/lib
    export LANG=en_US.utf8
    interproscan.sh --version | grep 5.54-87.0

    # Check dependencies
    /opt/interproscan/bin/blast/ncbi-blast-2.10.1+/rpsblast -version
    /opt/interproscan/bin/blast/ncbi-blast-2.10.1+/rpsbproc -version
    /opt/interproscan/bin/gene3d/4.3.0/cath-resolve-hits --version
    /opt/interproscan/bin/hmmer/hmmer2/2.3.2/hmmsearch -h
    /opt/interproscan/bin/hmmer/hmmer3/3.1b1/hmmpress -h
    /opt/interproscan/bin/hmmer/hmmer3/3.3/hmmscan -h
    /opt/interproscan/bin/mobidb/2.0/mobidb_lite.py -h
    /opt/interproscan/bin/ncoils/2.2.1/ncoils -h
    /opt/interproscan/bin/panther/panther_score.py 2>&1 | grep usage
    /opt/interproscan/bin/phobius/1.01/phobius.pl -h
    /opt/interproscan/bin/pirsf/3.10/pirsf.pl -help 2>&1 | grep fasta
    /opt/interproscan/bin/pirsr/0.1/pirsr.py --help
    /opt/interproscan/bin/prints/fingerPRINTScan 2>&1 | grep 3.596
    /opt/interproscan/bin/prosite/pfscan -h 2>&1 | grep "pfscan 2.3 revision 4"
    /opt/interproscan/bin/sfld/sfld_postprocess --help
    /opt/interproscan/bin/signalp/4.1/signalp -h
    /opt/interproscan/bin/superfamily/1.75/hmmscan.pl 2>&1 | grep "<hmm database>"
    /opt/interproscan/bin/tmhmm/2.0c/decodeanhmm -h 2>&1 | grep 1.1g
