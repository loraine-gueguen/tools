# QIAGEN CLC Assembly Cell

QIAGEN CLC Assembly Cell is a **commercial software**.

A license need to be purchased.

You need to generate on **THE** computing node where clc will be available a license file `.lic` using both `clc_cell_licutil` and your License Order ID

```
$ ./clc_cell_licutil

################################################################################
##                   CLC bio Assembly Cell License Utility                    ##
################################################################################
 Hostname:  cpu-node-XX
 Host ID(s): QYGVRF429ZFU,NNK32CM56K4O,XC3JNPKSTTUP,0QFM0RHA0311

Please select an option. Type the number and press return...
 (1) Request evaluation license.
 (2) Download license using a License Order ID.
> 2

Please enter (or copy/paste) your license Order-ID and press return...
> CLC-LICENSE-FOOBAR-CZRWA0O2LSZ40NKH2VSMDI5LXJY6TV9U

Downloading license with Order ID:
CLC-LICENSE-FOOBAR-CZRWA0O2LSZ40NKH2VSMDI5LXJY6TV9U

The license was successfully written to file:
/root/clc-assembly-cell-5.2.1-linux_64/licenses/CLC-LICENSE-FOOBAR-CZRWA0O2LSZ40NKH2VSMDI5LXJY6TV9U-2021-02-02.lic

$ mv licenses/CLC-LICENSE-FOOBAR-CZRWA0O2LSZ40NKH2VSMDI5LXJY6TV9U-2021-02-02.lic licenses/CLC-LICENSE-ASSEMBLY_CELL-2021-02-02.lic
```