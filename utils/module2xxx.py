#!/usr/bin/env python3

import subprocess

def module_avail(option = ""):
    cmd_module_avail = "module avail -t " + option
    module_avail = subprocess.check_output([cmd_module_avail], executable='/bin/bash', shell=True, encoding="utf-8").split("\n")

    softwares = {}

    for module in module_avail:
        if len(module) == 0 or module[0] == '/':
            continue
        module_split = module.split('/')
        software = module_split[0]
        version = "NA"
        if len(module_split) > 1:
            version = module_split[1]
        if software not in softwares.keys():
            softwares[software] = {}
            softwares[software]['versions'] = []

        softwares[software]['versions'].append(version)
    return(softwares)

def module_whatis(softwares_latest, software):

    software_latest_version = "/"+softwares_latest[software]["versions"][0]
    if software_latest_version == "/NA":
        software_latest_version = ""

    whatis_cmd = "module whatis "+software+software_latest_version
    whatiss = subprocess.check_output([whatis_cmd], executable='/bin/bash', shell=True, encoding="utf-8").split("\n")

    description = ""
    origin = ""
    for whatis in whatiss:
        whatis_split = whatis.split(':')
        if (len(whatis_split) < 2):
            continue
        if (len(whatis_split) > 2):
            if (whatis_split[1].strip() == "Origin"):
                origin = whatis_split[2].strip()
                continue
            if (whatis_split[1].strip() == "Description"):
                description = description + "\n" + " ".join(whatis_split[2:])
                continue
        else:
            description = "\n".join([description, " ".join(whatis_split[1:])])

    return([description.strip(),origin])

def module2dict():

    softwares = module_avail()
    softwares_latest = module_avail(option="-L")

    for software in softwares.keys():
        whatis = module_whatis(softwares_latest, software)
        #print(software,whatis)
        softwares[software]['description'] = whatis[0]
        softwares[software]['origin'] = whatis[1]
    return(softwares)

def dict2html_table(softwares):
    fileout = open('softwares.html', 'w')

    table = []
    table.append('''
    <!DOCTYPE html>
    <html>
    <head>
    <style>
    #customers {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td, #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #297fd5;
      color: white;
    }
    </style>
    </head>
    <body>
    ''')
    table.append('<table id="customers">')
    header = ['Name','Versions','Description','Origin']
    table.append('<tr><th>'+'</th><th>'.join(header)+'</th></tr>')
    for software in softwares.keys():
        description = softwares[software]['description']
        versions = ', '.join(softwares[software]['versions'])
        origin = softwares[software]['origin']
        table.append('<tr><td>'+software+'</td><td>'+description+'</td><td>'+versions+'</td><td>'+origin+'</td></tr>')

    table.append('</table>')

    table.append('''
    </body>
    </html>
    ''')

    fileout.writelines('\n'.join(table))
    fileout.close()


if __name__ == '__main__':
    softwares = module2dict()
    dict2html_table(softwares)
